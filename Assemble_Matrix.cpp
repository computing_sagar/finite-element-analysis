//
//  Assemble_Matrix.cpp
//  FEM
//
//  Created by Sagar Dolas on 04/06/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#include "Assemble_Matrix.hpp"

using namespace ::_COLSAMM_ ;
using namespace SIWIR2::FEM ;

SIWIR2::FEM::Assemble::Assemble(SIWIR2::FEM::MatrixMap<real_d> &A,SIWIR2::FEM::MatrixMap<real_d> &M,const SIWIR2::FEM::DomainData<real_d> &vertex,const SIWIR2::FEM::DomainData<real_l> &face , const real_l _numvertex,const real_l _numface,const real_d d ) : A_(&A),M_(&M),vertex_(&vertex),face_(&face),numvertex(_numvertex),numface(_numface),d(d) {
    
    corners.resize(6, 0.0) ;
    mylocalstiffmat_1.resize(3, {0.0,0.0,0.0}) ;
    mylocalstiffmat_2.resize(3, {0.0,0.0,0.0}) ;
    
    ELEMENTS::Triangle my_element ;
    
    // We need to parallise the assemble operation to see whether how it responds for the on ccnUMA systems 
    for (real_l iter =0 ; iter < numface; ++iter) {
        
        //Calculate the face index
        real_l findex = iter * geometricMap::faceindex ;
        
        //Access the vertices
        real_l v1 = (*face_)[findex] ;
        real_l v2 = (*face_)[findex+1] ;
        real_l v3 = (*face_)[findex+2] ;
        
        // Access the index of vertex in vertex dataset
        real_l vindex1 = v1 * geometricMap::vertexindex ;
        real_l vindex2 = v2 * geometricMap::vertexindex ;
        real_l vindex3 = v3 * geometricMap::vertexindex ;
        
        //Access the corner points
        corners[0] = (*vertex_)[vindex1] ;
        corners[1] = (*vertex_)[vindex1 + 1] ;
        corners[2] = (*vertex_)[vindex2] ;
        corners[3] = (*vertex_)[vindex2 + 1] ;
        corners[4] = (*vertex_)[vindex3] ;
        corners[5] = (*vertex_)[vindex3 + 1] ;
        
        //form local elemet
        my_element(corners) ;
        
        //Initialise delta
        delta(d) ;
        //Build local stiffness and local mass matrices
        mylocalstiffmat_1 = my_element.integrate(grad(v_()) * grad(w_()) ) ;
        mylocalstiffmat_2 = my_element.integrate(func<real_d>(ksqaure) * v_() * w_()) ;
        
        
        // Update the local stiffness matrices A
        for (real_l row = 0; row < 3; ++row) {
            for (real_l col = 0 ; col < 3 ; ++col){
                mylocalstiffmat_1[row][col] -= mylocalstiffmat_2[row][col] ;
            }
        }
        
        //Find out the actual position in global_stiffness_matrix
        std::vector<real_l> pos(3,0) ;
        
        pos[0] = v1 ;
        pos[1] = v2 ;
        pos[2] = v3 ;
        
        //Update their position in global stiffness Matrix
        for (real_l i = 0; i < 3; ++i) {
            for (real_l j = 0 ;j < 3; ++j ){
                (*A_)(pos[i],pos[j]) += mylocalstiffmat_1[i][j] ;
            }
        }
        
        // update directly in Compressed row storage or map
        mylocalstiffmat_1.resize(3, {0.0,0.0,0.0}) ;
        // Doing it for the mass matrix
        
        //Build local mass matrix
        mylocalstiffmat_1 = my_element.integrate(v_() * w_()) ;
        
        //Update their position in global mass Matrix
        for (real_l i = 0; i < 3; ++i) {
            for (real_l j = 0 ;j < 3; ++j ){
                (*M_)(pos[i],pos[j]) += mylocalstiffmat_1[i][j] ;
            }
        }

    }
}

SIWIR2::FEM::Assemble::~Assemble(){
    
    
}
